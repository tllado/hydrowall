// display.h
//
// Display functions for TI TM4C123GXL using Keil v5
// Displays data on ST7735 using a TI TM4C123GXL microcontroller
//
// This file is part of hydrowall v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-03-09

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

// display_init()
// Initializes ST7735 display
void display_init(void);

// display_update()
// Updates ST7735 display
void display_update(const uint32_t time, const uint32_t grow_lights_state,
    const uint32_t uv_lights_state, const uint32_t water_pump_state,
    const float ec_value, const uint32_t ec_pump_state, const float ph_value,
    const uint32_t ph_pump_state);

////////////////////////////////////////////////////////////////////////////////
// End of file
