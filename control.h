// control.h
//
// Load control for TI TM4C123GXL using Keil v5
// A simple library that controls loads per a timer schedule using GPIO pins on
// a TI TM4C123GXL microcontroller
//
// This file is part of hydrowall v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-05-23

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "lladoware/clock.h"
#include "lladoware/output_pins.h"
#include "config.h"

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

uint32_t ec_dose(const float ec_value);
void ec_init(void);
void ec_instrument_off(void);
void ec_instrument_on(void);
void ec_pump_off(void);
void ec_pump_on(void);
float ec_read(void);

void grow_lights_init(void);
void grow_lights_off(void);
void grow_lights_on(void);

uint32_t ph_dose(const float ph_value);
void ph_init(void);
void ph_instrument_off(void);
void ph_instrument_on(void);
void ph_pump_off(void);
void ph_pump_on(void);
float ph_read(void);

void uv_lights_init(void);
void uv_lights_off(void);
void uv_lights_on(void);

void water_pump_init(void);
void water_pump_off(void);
void water_pump_on(void);

////////////////////////////////////////////////////////////////////////////////
// End of file
