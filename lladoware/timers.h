// lladoware_timers.h
//
// Timers for TI TM4C123GXL using Keil v5
// Receive setup parameters and pointer to a task to be run
//
// This file is part of lladoware v1.1
// Travis Llado, travis@travisllado.com
// Last modified 2021-03-05

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

// Timer1_Init()
// Initializes Timer1 to run user-specified task
uint32_t Timer1_Init(const uint32_t period, const uint32_t priority, void(*task)(void));

// Timer2_Init()
// Initializes Timer2 to run user-specified task
uint32_t Timer2_Init(const uint32_t period, const uint32_t priority, void(*task)(void));

////////////////////////////////////////////////////////////////////////////////
// End of file
