// lladoware_interrupts.h
// Interrupt management functions for TI TM4C123GXL, using Keil v5

// This file is part of lladoware v1
// Travis Llado, travis@travisllado.com
// Last modified 2021-03-05

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts

void WaitForInterrupt(void);  // low power mode
