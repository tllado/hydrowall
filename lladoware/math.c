// math.c

#include <stdint.h>

void swap(uint32_t *p, uint32_t *q) {
   uint32_t t;

   t = *p;
   *p = *q;
   *q = t;
}

void sort(uint32_t a[], const uint32_t n) {
   uint32_t i, j;

   for(i = 0; i < n - 1; i++) {
      for(j = 0; j < n - i - 1; j++) {
         if(a[j] > a[j + 1])
            swap(&a[j], &a[j+1]);
      }
   }
}

uint32_t mean(const uint32_t list[], const uint32_t length) {
  uint32_t sum = 0;

  for(int i = 0; i < length; i++) {
    sum += list[i];
  }

  return sum / length;
}

uint32_t median(uint32_t list[], const uint32_t length) {
  sort(list, length);
  uint32_t middle = ((length + 1) / 2) - 1;

  return list[middle];
}

////////////////////////////////////////////////////////////////////////////////
// End of file
