// control.h
//
// Load control for TI TM4C123GXL using Keil v5
// A simple library that controls loads per a timer schedule using GPIO pins on
// a TI TM4C123GXL microcontroller
//
// This file is part of hydrowall v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-04-04

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "control.h"
#include "lladoware/adc.h"
#include "lladoware/math.h"

////////////////////////////////////////////////////////////////////////////////
// ec_dose()
// Returns dosing pump runtime based on argument

uint32_t ec_dose(const float ec_value) {
  return (ec_value < EC_TARGET) ?
      ((EC_TARGET - ec_value)*EC_DOSE_GAIN) :
      0;
}

////////////////////////////////////////////////////////////////////////////////
// ec_control_init()
// Initializes pins required for EC sensor reading and EC pump control

void ec_init(void) {
  adc_init(EC_SENSOR_CHANNEL);
  init_pin(EC_PUMP_PIN, 0);
  init_pin(EC_INSTRUMENT_PIN, 0);
  update_pin(EC_INSTRUMENT_PIN, PIN_STATE_HIGH);
}

////////////////////////////////////////////////////////////////////////////////
// ec_dose()
// Actuates EC pump according to EC level
// Returns EC pump runtime

void ec_instrument_off(void) {
  update_pin(EC_INSTRUMENT_PIN, PIN_STATE_LOW);
}

////////////////////////////////////////////////////////////////////////////////
// ec_dose()
// Actuates EC pump according to EC level
// Returns EC pump runtime

void ec_instrument_on(void) {
  update_pin(EC_INSTRUMENT_PIN, PIN_STATE_HIGH);
}

////////////////////////////////////////////////////////////////////////////////
// ec_dose()
// Actuates EC pump according to EC level
// Returns EC pump runtime

void ec_pump_off(void) {
  update_pin(EC_PUMP_PIN, PIN_STATE_LOW);
}

////////////////////////////////////////////////////////////////////////////////
// ec_dose()
// Actuates EC pump according to EC level
// Returns EC pump runtime

void ec_pump_on(void) {
  update_pin(EC_PUMP_PIN, DOSING_PUMP_DUTY);
}

////////////////////////////////////////////////////////////////////////////////
// ec_read()
// Returns most recent EC reading

float ec_read(void) {
  static uint32_t ec_raw_buffer[EC_FILTER_LENGTH];
  static uint32_t ec_median_buffer[EC_FILTER_LENGTH];
  static uint32_t ec_buffer_position = 0;

  uint32_t buffer_copy[EC_FILTER_LENGTH];
  for(uint32_t i = 0; i < EC_FILTER_LENGTH; i++) {
    buffer_copy[i] = ec_raw_buffer[i];
  }

  ec_raw_buffer[ec_buffer_position] = adc_read(EC_SENSOR_CHANNEL);
  ec_median_buffer[ec_buffer_position] = median(buffer_copy, EC_FILTER_LENGTH);
  ec_buffer_position = (ec_buffer_position + 1) % EC_FILTER_LENGTH;

  const float m = EC_SENSOR_CAL_SLOPE;
  const float x = mean(ec_median_buffer, EC_FILTER_LENGTH);
  const float b = EC_SENSOR_CAL_OFFSET;
  const float ec_filtered_reading = m*x + b;

  return ec_filtered_reading;
}

////////////////////////////////////////////////////////////////////////////////
// grow_lights_init()
// initializes pin required for grow light control

void grow_lights_init(void) {
  init_pin(GROW_LIGHTS_PIN, 0);
}

////////////////////////////////////////////////////////////////////////////////
// grow_lights_on()
// run grow lights on predetermined schedule

void grow_lights_on(void) {
  update_pin(GROW_LIGHTS_PIN, PIN_STATE_HIGH);
}

////////////////////////////////////////////////////////////////////////////////
// grow_lights_off()
// run grow lights on predetermined schedule

void grow_lights_off(void) {
  update_pin(GROW_LIGHTS_PIN, PIN_STATE_LOW);
}

////////////////////////////////////////////////////////////////////////////////
// ph_control_update()
// Actuates pH pump according to pH level and time
// Returns pH pump runtime

uint32_t ph_dose(const float ph_value) {
  return (ph_value > PH_TARGET) ?
      ((ph_value - PH_TARGET)*PH_DOSE_GAIN) :
      0;
}

////////////////////////////////////////////////////////////////////////////////
// ph_control_init()
// Initializes pins required for ph sensor reading and ph pump control

void ph_init(void) {
  adc_init(PH_SENSOR_CHANNEL);
  init_pin(PH_PUMP_PIN, 0);
  init_pin(PH_INSTRUMENT_PIN, 0);
  update_pin(PH_INSTRUMENT_PIN, PIN_STATE_HIGH);
}

////////////////////////////////////////////////////////////////////////////////
// ec_dose()
// Actuates EC pump according to EC level
// Returns EC pump runtime

void ph_instrument_off(void) {
  update_pin(PH_INSTRUMENT_PIN, PIN_STATE_LOW);
}

////////////////////////////////////////////////////////////////////////////////
// ec_dose()
// Actuates EC pump according to EC level
// Returns EC pump runtime

void ph_instrument_on(void) {
  update_pin(PH_INSTRUMENT_PIN, PIN_STATE_HIGH);
}

////////////////////////////////////////////////////////////////////////////////
// ec_dose()
// Actuates EC pump according to EC level
// Returns EC pump runtime

void ph_pump_off(void) {
  update_pin(PH_PUMP_PIN, PIN_STATE_LOW);
}

////////////////////////////////////////////////////////////////////////////////
// ec_dose()
// Actuates EC pump according to EC level
// Returns EC pump runtime

void ph_pump_on(void) {
  update_pin(PH_PUMP_PIN, DOSING_PUMP_DUTY);
}

////////////////////////////////////////////////////////////////////////////////
// ph_read()
// Returns most recent pH reading

float ph_read(void) {
  static uint32_t ph_raw_buffer[PH_FILTER_LENGTH];
  static uint32_t ph_median_buffer[PH_FILTER_LENGTH];
  static uint32_t ph_buffer_position = 0;

  uint32_t buffer_copy[PH_FILTER_LENGTH];
  for(uint32_t i = 0; i < PH_FILTER_LENGTH; i++) {
    buffer_copy[i] = ph_raw_buffer[i];
  }

  ph_raw_buffer[ph_buffer_position] = adc_read(PH_SENSOR_CHANNEL);
  ph_median_buffer[ph_buffer_position] = median(buffer_copy, PH_FILTER_LENGTH);
  ph_buffer_position = (ph_buffer_position + 1) % PH_FILTER_LENGTH;

  const float m = PH_SENSOR_CAL_SLOPE;
  const float x = mean(ph_median_buffer, PH_FILTER_LENGTH);
  const float b = PH_SENSOR_CAL_OFFSET;
  const float ph_filtered_reading = m*x + b;

  return ph_filtered_reading;
}

////////////////////////////////////////////////////////////////////////////////
// uv_lights_init()
// initializes pin required for uv light control

void uv_lights_init(void) {
  init_pin(UV_LIGHTS_PIN, 0);
}

////////////////////////////////////////////////////////////////////////////////
// uv_lights_on()
// run uv lights on predetermined schedule

void uv_lights_on(void) {
  update_pin(UV_LIGHTS_PIN, PIN_STATE_HIGH);
}

////////////////////////////////////////////////////////////////////////////////
// uv_lights_off()
// run uv lights on predetermined schedule

void uv_lights_off(void) {
  update_pin(UV_LIGHTS_PIN, PIN_STATE_LOW);
}

////////////////////////////////////////////////////////////////////////////////
// water_pump_init()
// Initializes pin required for water pump control

void water_pump_init(void) {
  init_pin(WATER_PUMP_PIN, 0);
}

////////////////////////////////////////////////////////////////////////////////
// water_pump_update()
// Run water pump on predetermined schedule
// Returns whether water pump is running

void water_pump_off(void) {
  update_pin(WATER_PUMP_PIN, PIN_STATE_LOW);
}

////////////////////////////////////////////////////////////////////////////////
// water_pump_update()
// Run water pump on predetermined schedule
// Returns whether water pump is running

void water_pump_on(void) {
  update_pin(WATER_PUMP_PIN, PIN_STATE_HIGH);
}

////////////////////////////////////////////////////////////////////////////////
// End of file
