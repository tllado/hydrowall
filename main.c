// main.c
// Hydroponics control program for TI TM4C123GXL, using Keil v5
// Controls various subsystems required to automate hydroponics operation

// This file is part of Hydrowall
// Travis Llado, travis@travisllado.com
// Last modified 2021-03-05

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "lladoware/tm4c123gh6pm.h"
#include "lladoware/buttons.h"
#include "lladoware/clock.h"
#include "lladoware/interrupts.h"
#include "lladoware/PLL.h"
#include "lladoware/system_config.h"
#include "lladoware/timers.h"
#include "control.h"
#include "display.h"

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void programs_init(void);
void programs_update(void);

////////////////////////////////////////////////////////////////////////////////
// Global Constants

#define BUTTON_PRIORITY           3

#define DISPLAY_UPDATE_FREQUENCY  1 // Hz, must be divisor of PRGM_TICK_FREQ

#define PROGRAM_TICK_FREQUENCY    10  // Hz
#define PROGRAM_TICK_PERIOD_SYS   SYS_FREQ/PROGRAM_TICK_FREQUENCY
#define PROGRAM_TICK_PERIOD_CLK   CLOCK_SECOND/PROGRAM_TICK_FREQUENCY
#define PROGRAM_TICK_PRIORITY     2

////////////////////////////////////////////////////////////////////////////////
// int main()

int main(void) {
  // Initialize all hardware
  PLL_Init();
  programs_init();
  clock_init();
  button_left_init(BUTTON_PRIORITY, &clock_increment_hr);
  button_right_init(BUTTON_PRIORITY, &clock_increment_min);
  Timer2_Init(PROGRAM_TICK_PERIOD_SYS, PROGRAM_TICK_PRIORITY, &programs_update);

  // All further actions performed by interrupt handlers
  while(1) {
    WaitForInterrupt();
  }
}

////////////////////////////////////////////////////////////////////////////////
// programs_init()
// Run initialization actions for control programs

void programs_init(void) {
  display_init();
  ec_init();
  grow_lights_init();
  ph_init();
  uv_lights_init();
  water_pump_init();
}

////////////////////////////////////////////////////////////////////////////////
// programs_update()
// Run update actions for control programs

void programs_update(void) {
  const uint32_t time_now = clock_time();

  // Update grow lights
  uint32_t grow_lights_status = STOPPED;
  if((time_now % GROW_LIGHTS_PERIOD - GROW_LIGHTS_OFFSET) < GROW_LIGHTS_DUTY) {
    grow_lights_on();
    grow_lights_status = RUNNING;
  } else {
    grow_lights_off();
  }

  // Update uv lights
  uint32_t uv_lights_status = STOPPED;
  if((time_now % UV_LIGHTS_PERIOD - UV_LIGHTS_OFFSET) < UV_LIGHTS_DUTY) {
    uv_lights_on();
    uv_lights_status = RUNNING;
  } else {
    uv_lights_off();
  }

  // Update EC control
  static float ec_value = 0.0F;
  static uint32_t ec_pump_shutdown = 0;
  uint32_t ec_pump_status = STOPPED;

  if(((time_now / CLOCK_MINUTE) % 2 == 0) && (clock_secs() > 0)) {
    ec_instrument_on();
    if (((time_now % CLOCK_MINUTE) / CLOCK_SECOND) > EC_SENSOR_RESET_TIME) {
      ec_value = ec_read();
    }
  } else {
    ec_instrument_off();
  }

  if(((time_now % EC_DOSE_PERIOD - EC_DOSE_OFFSET) < PROGRAM_TICK_PERIOD_CLK) &&
      (grow_lights_status == RUNNING)) {
    ec_pump_shutdown = time_now + ec_dose(ec_value);
  }
  if(ec_pump_shutdown > time_now){
    ec_pump_on();
    ec_pump_status = ec_pump_shutdown - time_now;
  } else {
    ec_pump_off();
  }

  // Update pH control
  static float ph_value = 0.0F;
  static uint32_t ph_pump_shutdown = 0;
  uint32_t ph_pump_status = STOPPED;

  if(((time_now / CLOCK_MINUTE) % 2 == 1) && (clock_secs() > 0)) {
    ph_instrument_on();
    if (((time_now % CLOCK_MINUTE) / CLOCK_SECOND) > PH_SENSOR_RESET_TIME) {
      ph_value = ph_read();
    }
  } else {
    ph_instrument_off();
  }

  if(((time_now % PH_DOSE_PERIOD - PH_DOSE_OFFSET) < PROGRAM_TICK_PERIOD_CLK) &&
      (grow_lights_status == RUNNING)) {
    ph_pump_shutdown = time_now + ph_dose(ph_value);
  }
  if(ph_pump_shutdown > time_now){
    ph_pump_on();
    ph_pump_status = ph_pump_shutdown - time_now;
  } else {
    ph_pump_off();
  }

  // Update water pump
  uint32_t water_pump_status = STOPPED;
  if(((time_now % WATER_PUMP_PERIOD - WATER_PUMP_OFFSET) < WATER_PUMP_DUTY) &&
      (grow_lights_status == RUNNING)) {
    water_pump_on();
    water_pump_status = RUNNING;
  } else {
    water_pump_off();
  }

  // Update display
  if((time_now % (CLOCK_SECOND / DISPLAY_UPDATE_FREQUENCY)) <
      PROGRAM_TICK_PERIOD_CLK){
    display_update(time_now, grow_lights_status, uv_lights_status,
        water_pump_status, ec_value, ec_pump_status, ph_value, ph_pump_status);
  }
}

////////////////////////////////////////////////////////////////////////////////
// End of file
