// display.c
//
// Display functions for TI TM4C123GXL using Keil v5
// Displays data on ST7735 using a TI TM4C123GXL microcontroller
//
// This file is part of hydrowall v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-03-09

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include "lladoware/clock.h"
#include "ST7735.h"

////////////////////////////////////////////////////////////////////////////////
// Internal Constants

#define CHAR_WIDTH  6
#define CHAR_HEIGHT 10

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void display_param_float(const char* name, const float value,
    const uint32_t line, const uint32_t tab, const uint32_t color);
void display_param_uint(const char* name, const uint32_t value,
    const uint32_t line, const uint32_t tab, const uint32_t color);
void display_time(const uint32_t time, const uint32_t line,
    const uint32_t space, const uint32_t color);
void display_time_and_date(const uint32_t time, const uint32_t line,
    const uint32_t space, const uint32_t color);

////////////////////////////////////////////////////////////////////////////////
// display_init()
// Initializes ST7735 display

void display_init(void) {
  ST7735_Init();
}

////////////////////////////////////////////////////////////////////////////////
// display_param_float()
// Prints a parameter name and float value on a single line

void display_param_float(const char* name, const float value,
    const uint32_t line, const uint32_t tab, const uint32_t color) {
  const uint32_t characteristic = value;
  uint32_t copy = characteristic;
  uint32_t length = 1;
  copy /= 10;
  while(copy != 0){
    length++;
    copy /= 10;
  }
  const uint32_t mantissa = (value - characteristic)*1000;

  ST7735_print_string(line, 0, name, color);
  ST7735_FillRect((tab+1)*CHAR_WIDTH, line*CHAR_HEIGHT, 6*CHAR_WIDTH,
      CHAR_HEIGHT, ST7735_BLACK);
  ST7735_print_uint(line, tab, characteristic, color);
  ST7735_print_string(line, (tab + length), ".", color);
  ST7735_print_uint(line, (tab + length + 1), mantissa, color);
}

////////////////////////////////////////////////////////////////////////////////
// display_param_uint()
// Prints a parameter name and uint value on a single line

void display_param_uint(const char* name, const uint32_t value,
    const uint32_t line, const uint32_t tab, const uint32_t color) {
  ST7735_print_string(line, 0, name, color);
  ST7735_FillRect((tab+1)*CHAR_WIDTH, line*CHAR_HEIGHT, 4*CHAR_WIDTH,
      CHAR_HEIGHT, ST7735_BLACK);
  ST7735_print_uint(line, tab, value, color);
}

////////////////////////////////////////////////////////////////////////////////
// display_time()
// Prints hr::mn::sc to display on a single line

void display_time(const uint32_t time, const uint32_t line,
    const uint32_t space, const uint32_t color) {
  const uint32_t hours = time / CLOCK_HOUR;
  if(hours < 10) {
    ST7735_print_uint(line, 3, 0, ST7735_WHITE);
    ST7735_print_uint(line, 4, hours, ST7735_WHITE);
  } else {
    ST7735_print_uint(line, 3, hours, ST7735_WHITE);
  }

  ST7735_print_string(line, 5, ":", ST7735_WHITE);

  const uint32_t minutes = (time % CLOCK_HOUR) / CLOCK_MINUTE;
  if(minutes < 10) {
    ST7735_print_uint(line, 6, 0, ST7735_WHITE);
    ST7735_print_uint(line, 7, minutes, ST7735_WHITE);
  } else {
    ST7735_print_uint(line, 6, minutes, ST7735_WHITE);
  }

  ST7735_print_string(line, 8, ":", ST7735_WHITE);

  const uint32_t seconds = (time % CLOCK_MINUTE) / CLOCK_SECOND;
  if(seconds < 10) {
    ST7735_print_uint(line, 9, 0, ST7735_WHITE);
    ST7735_print_uint(line, 10, seconds, ST7735_WHITE);
  } else {
    ST7735_print_uint(line, 9, seconds, ST7735_WHITE);
  }
}

////////////////////////////////////////////////////////////////////////////////
// display_time_and_day()
// Prints dy::hr::mn::sc to display on a single line

void display_time_and_day(const uint32_t time, const uint32_t line,
    const uint32_t space, const uint32_t color) {
  const uint32_t days = time / CLOCK_DAY;
  if(days < 10) {
    ST7735_print_uint(line, 0, 0, ST7735_WHITE);
    ST7735_print_uint(line, 1, days, ST7735_WHITE);
  } else {
    ST7735_print_uint(line, 0, days, ST7735_WHITE);
  }

  ST7735_print_string(line, 2, ":", ST7735_WHITE);

  display_time((time % CLOCK_DAY), line, space, color);
}

////////////////////////////////////////////////////////////////////////////////
// display_update()
// Updates ST7735 display

void display_update(const uint32_t time, const uint32_t grow_lights_state,
    const uint32_t uv_lights_state, const uint32_t water_pump_state,
    const float ec_value, const uint32_t ec_pump_state, const float ph_value,
    const uint32_t ph_pump_state) {
  const uint32_t tab_length = 12;
  uint32_t line_num = 0;

  display_time_and_day(time, line_num, 0, ST7735_WHITE);
  ++line_num;
  display_param_uint("grow lights", grow_lights_state, ++line_num, tab_length,
      ST7735_YELLOW);
  ++line_num;
  display_param_uint("uv lights", uv_lights_state, ++line_num, tab_length,
      ST7735_RED);
  ++line_num;
  display_param_uint("water pump", water_pump_state, ++line_num, tab_length,
      ST7735_CYAN);
  ++line_num;
  display_param_float("ec reading", ec_value, ++line_num, tab_length,
      ST7735_GREEN);
  display_param_uint("ec pump", ec_pump_state, ++line_num, tab_length,
      ST7735_GREEN);
  ++line_num;
  display_param_float("ph reading", ph_value, ++line_num, tab_length,
      ST7735_MAGENTA);
  display_param_uint("ph pump", ph_pump_state, ++line_num, tab_length,
      ST7735_MAGENTA);

}

////////////////////////////////////////////////////////////////////////////////
// End of file
