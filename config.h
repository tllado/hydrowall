// config.h
//
// Configuration parameters for a Travis' hydroponics system
//
// This file is part of hydrowall v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-05-23

////////////////////////////////////////////////////////////////////////////////
// Constants

#define RUNNING 1
#define STOPPED 0

#define DOSING_PUMP_DUTY      ((PWM_PERIOD_DEFAULT - 1) * 5 / 12) // ~1ml/sec

#define FILL_DRAIN_PERIOD     (4 * CLOCK_HOUR)

#define EC_DOSE_GAIN          3000.0F // ms/point_ec
#define EC_DOSE_OFFSET        (1 * CLOCK_MINUTE)
#define EC_DOSE_PERIOD        FILL_DRAIN_PERIOD
#define EC_FILTER_LENGTH      10
#define EC_INSTRUMENT_PIN     PB4
#define EC_PUMP_PIN           PB6
#define EC_SENSOR_CAL_OFFSET  0.32977F
#define EC_SENSOR_CAL_SLOPE   0.0050968F
#define EC_SENSOR_CHANNEL     1 // PE2
#define EC_SENSOR_RESET_TIME  1 // seconds
#define EC_TARGET             2.0F

#define GROW_LIGHTS_DUTY      (16 * CLOCK_HOUR)
#define GROW_LIGHTS_OFFSET    (6 * CLOCK_HOUR)
#define GROW_LIGHTS_PERIOD    (1 * CLOCK_DAY)
#define GROW_LIGHTS_PIN       PB0

#define PH_DOSE_GAIN          2000.0F // ms/point_ph
#define PH_DOSE_OFFSET        (1 * CLOCK_MINUTE)
#define PH_DOSE_PERIOD        FILL_DRAIN_PERIOD
#define PH_FILTER_LENGTH      10
#define PH_INSTRUMENT_PIN     PB1
#define PH_PUMP_PIN           PB7
#define PH_SENSOR_CAL_OFFSET  17.006F
#define PH_SENSOR_CAL_SLOPE   -0.0054383F
#define PH_SENSOR_CHANNEL     0 // PE3
#define PH_SENSOR_RESET_TIME  1 // seconds
#define PH_TARGET             6.0F

#define UV_LIGHTS_DUTY        (2 * CLOCK_HOUR)
#define UV_LIGHTS_OFFSET      (1 * CLOCK_HOUR)
#define UV_LIGHTS_PERIOD      GROW_LIGHTS_PERIOD
#define UV_LIGHTS_PIN         PB5

#define WATER_PUMP_DUTY       (5 * CLOCK_MINUTE)
#define WATER_PUMP_OFFSET     0
#define WATER_PUMP_PERIOD     FILL_DRAIN_PERIOD
#define WATER_PUMP_PIN        PB3

////////////////////////////////////////////////////////////////////////////////
// End of file
